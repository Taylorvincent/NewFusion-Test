var express = require('express');
var router = express.Router();

var viewsController = require('../controllers/views');

/* GET home page. */
router.get('/', viewsController.index);

router.get('/videos/:id', viewsController.detail);

module.exports = router;

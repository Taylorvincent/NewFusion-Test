(function() {
  function setVideoSource(){
    if (document.getElementById("videoSource")) {
      let url = "https://s3.eu-west-3.amazonaws.com/vincent.newfusion.test/videos/"
      key = document.getElementById("videoKey").innerHTML;
      cleanKey = key.split(' ').join('+');
      url += cleanKey;
      document.getElementById("videoSource").setAttribute("src", url)
    }
  }

  setVideoSource()
})()
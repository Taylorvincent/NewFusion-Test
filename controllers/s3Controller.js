var AWS = require('aws-sdk');
var s3 = new AWS.S3();
var myBucket = 'vincent.newfusion.test';

// https://github.com/aws/aws-sdk-js/issues/1181
// https://gist.github.com/mihaiserban/1f35d488405812f2bbd4b16e38e4afb5

function getS3ObjectHead (filename) {
    const params = {
      Bucket: myBucket,
      Key: filename,
    };
  
    return s3.headObject(params).promise();
  }

exports.getS3ObjectHead = getS3ObjectHead;

exports.ListS3Objects = async function (params, s3DataContents, includeHead) {
  try {
      params.Bucket = myBucket;
      const data = await s3.listObjectsV2(params).promise();

      let contents = data.Contents;
      if (includeHead) {
          for (let i = 0; i < contents.length; i++) {
              let element = contents[i];
  
              const fileObject = await getS3ObjectHead(element.Key)
              element.head = fileObject
          }
      }

      s3DataContents = s3DataContents.concat(contents);
      if (data.IsTruncated) {
          // Set Marker to last returned key
          params.Marker = contents[contents.length-1].Key;
          await ListS3Objects(params, s3DataContents);
      } else {
          return s3DataContents;
      }
  } catch (error) { 
      throw error
  }
}

exports.GetS3ObjectStream = function(key){
    const params = {
        Bucket: myBucket,
        Key: key,
      };
    return s3.getObject(params).createReadStream();
}
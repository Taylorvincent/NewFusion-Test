const s3Controller = require('./s3Controller');
const fs = require("fs");

exports.get_list = async (req, res, next) => {
  
  var params = {
    StartAfter: 'videos/',
  };

  let s3DataContents = []; 

  try {
    const data = await s3Controller.ListS3Objects(params, s3DataContents)
    const sortedData = data.sort( (a , b) => {
      return Date.parse(b.LastModified) - Date.parse(a.LastModified); //newest first
    });
    res.status('200').json({
      message: "ok",
      videos: sortedData
    });
  } catch (error) {
    console.log( 'error', error );
    res.status('500').json({
      message: "error",
      data: error
    });
  }

}

exports.get_list_meta = async (req, res, next) => {
  
  let params = {
    StartAfter: 'videos/',
  };

  let s3DataContents = []; 

  try {
    const includeHead = true
    const data = await s3Controller.ListS3Objects(params, s3DataContents, includeHead)
    const sortedData = data.sort( (a , b) => {
      return Date.parse(b.LastModified) - Date.parse(a.LastModified); //newest first
    });
    res.status('200').json({
      message: "ok",
      videos: sortedData
    });
  } catch (error) {
    console.log( 'error', error );
    res.status('500').json({
      message: "error",
      data: error
    });
  }

}

// exports.get_video_meta = async (req, res, next) => {
//   const file_head = await s3Controller.getS3ObjectHead(req.params.id + ".mp4")
//   res.status("200").json({video: file_head})
// }

// exports.get_video = async (req, res, next) => {
//   console.log( '"videos/" + req.params.id + ".mp4"', "videos/" + req.params.id);
//   // let dataStream = s3Controller.GetS3ObjectStream("videos/" + req.params.id);
//   // dataStream.pipe(res);
  
//   const path = 'assets/sample.mp4'
//   const stat = fs.statSync(path)
//   const fileSize = stat.size
//   const range = req.headers.range
//   if (range) {
//     const parts = range.replace(/bytes=/, "").split("-")
//     const start = parseInt(parts[0], 10)
//     const end = parts[1] 
//       ? parseInt(parts[1], 10)
//       : fileSize-1
//     const chunksize = (end-start)+1
//     const file = fs.createReadStream(path, {start, end})
//     const head = {
//       'Content-Range': `bytes ${start}-${end}/${fileSize}`,
//       'Accept-Ranges': 'bytes',
//       'Content-Length': chunksize,
//       'Content-Type': 'video/mp4',
//     }
//     res.writeHead(206, head);
//     file.pipe(res);
//   } else {
//     const head = {
//       'Content-Length': fileSize,
//       'Content-Type': 'video/mp4',
//     }
//     res.writeHead(200, head)
//     fs.createReadStream(path).pipe(res)
//   }
// }

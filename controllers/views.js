const axios = require("axios");
const s3Controller = require("./s3Controller");

exports.index = (req, res, next) => {
  res.render('index', { title: 'NewFusionTube' });
}

exports.detail = async (req, res, next) => {
  const key = "videos/"+req.params.id + ".mp4"
  try {
    const head = await s3Controller.getS3ObjectHead(key);
    res.render('detail', {title: head.Metadata.title, videoKey: req.params.id + ".mp4"});
  }
  catch(e){
    console.log( 'e', e );
    res.render('notFound');
  }
}
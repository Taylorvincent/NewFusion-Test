var express = require('express');
var router = express.Router();

var apiController = require('../controllers/api');

/* GET videos list */
router.get('/videos', apiController.get_list);
// router.get('/videos/:id', apiController.get_video);

router.get('/videos_meta', apiController.get_list_meta);
// router.get('/videos_meta/:id', apiController.get_video_meta);

router.get('/test', (req, res) => {
  return res.status("200").json({data: "test"})
})

module.exports = router;

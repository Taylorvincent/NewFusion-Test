
(function() {

  let latestVideoDate = 0;
  let videosCount = 0;

  function setVideoContainer(element){
    let container = document.getElementById("videosContainer");
    container.removeChild(container.childNodes[0]);
    container.appendChild(element);
  }

  function renderVideos(data){
    let ul = document.createElement('ul');
    for (let i = 0; i < data.length; i++) {
      const video = data[i];
      let li = document.createElement('li');
      let a = document.createElement('a');
      a.innerHTML = video.head.Metadata.title;
      a.href = video.Key.slice(0, -4);
      li.appendChild(a);
      ul.appendChild(li);
    }
    setVideoContainer(ul);
  }

  function fetchVideosList(){
    axios.get('/api/v1/videos_meta')
      .then(function (response) {
        console.log(response);
        renderVideos(response.data.videos);
        latestVideoDate = Date.parse(response.data.videos[0].LastModified);
        videosCount = response.data.videos.length;
        console.log( 'latestVideoDate', latestVideoDate );
      })
      .catch(function (error) {
        let error = document.createElement('p');
        error.innerHTML = "error while fetching videos"
        setVideoContainer(error);
        console.log(error);
      })
  }


  // Fire initial functions. 

  fetchVideosList();

  setInterval(() => {
    console.log( 'checking for new videos...' );
    axios.get('/api/v1/videos')
      .then(function(response){
        const newLatestDate = Date.parse(response.data.videos[0].LastModified);
        const newCount = response.data.videos.length;
        if ( newLatestDate > latestVideoDate || videosCount != newCount) {
          fetchVideosList()
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    }, 30000
  )

})();